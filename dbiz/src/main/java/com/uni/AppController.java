package com.uni;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

//import org.springframework.data.repository.query.Param;

import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
//import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.param.checkout.SessionCreateParams;
import com.stripe.model.checkout.Session;

import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentCreateParams;

@Controller
public class AppController {

	@Value("${baseUrl}")
	private String baseUrl;				

	@Value("${stripe.public.key}")
	private String stripePublicKey;		
	
    @GetMapping("/")
    public String viewMainPage() {
        return "main";
    } 
    
    @GetMapping("/solution_acqusition")
    public String solution_acqusition() {
        return "solution_acqusition";
    }

    @GetMapping("/solution_validation")
    public String solution_validation() {
        return "solution_validation";
    }

    @GetMapping("/solution_activation")
    public String solution_activation() {
        return "solution_activation";
    }   
    
    @GetMapping("/solution_retention")
    public String solution_retention() {
        return "solution_retention";
    }
    
    @GetMapping("/solution_revenue")
    public String solution_revenue() {
        return "solution_revenue";
    }

    @GetMapping("/let_talk")
    public String let_talk() {
        return "let_talk";
    }    

    @GetMapping("/client")
    public String client() {
        return "client";
    }
    
    @GetMapping("/about")
    public String about() {
        return "about";
    }

    @GetMapping("/shopping_cart")
    public String shopping_cart(Model model) {
    	model.addAttribute("product1_price", 7800.00);
    	model.addAttribute("product2_price", 7800.00);
    	model.addAttribute("product1_name", "Pickeers (6 months Service)");
    	model.addAttribute("product2_name", "Mission Pick (12 months Sevice)");    	
		/*
		 * model.addAttribute("product3_price", 300.00);
		 * model.addAttribute("product4_price", 400.00);
		 * model.addAttribute("product5_price", 500.00);
		 * model.addAttribute("product6_price", 600.00);
		 */
    	
    	model.addAttribute("baseUrl", baseUrl);
    	
        return "shopping_cart";
    }
    
    @GetMapping("/shopping_cart_error")
    public String shopping_cart_error() {
    	
        return "shopping_cart_error";
    }
    
    @GetMapping("/shopping_cart_checkout")
    public String shopping_cart_checkout(Model model, 
    		@RequestParam(name = "total_price") String totalPrice,
    		@RequestParam(name = "tel") String tel,
    		@RequestParam(name = "name") String name,
    		@RequestParam(name = "email") String email
    		) {
    	
    	model.addAttribute("baseUrl", baseUrl);
    	model.addAttribute("stripePublicKey", stripePublicKey);
    	model.addAttribute("totalPrice", totalPrice);
    	model.addAttribute("name", name);
    	model.addAttribute("tel", tel);
    	model.addAttribute("email", email);
    	
    	Float totalPriceFloat = Float.parseFloat(totalPrice)/100;
    	String totalPriceString = String.format("%.02f", totalPriceFloat);
    	model.addAttribute("totalPriceString", totalPriceString);
    	
        return "checkout";
    }   
 
    @GetMapping("/shopping_cart_success")
    public String shopping_cart_success() {
    	
        return "shopping_cart_success";
    }    
    
    @GetMapping("/register_login")
    public String register_login() {
    	
        return "register_login";
    }    
    
    @GetMapping("/send_mail")
    public String send_mail(@RequestParam(name = "email") String email) {
    	
		
		/*
		 * Properties props = new Properties(); props.put("mail.smtp.auth", "true");
		 * props.put("mail.smtp.starttls.enable", "true");
		 * //props.put("mail.smtp.host","smtp.gmail.com"); props.put("mail.smtp.host",
		 * "smtp-mail.outlook.com"); props.put("mail.smtp.port", "587");
		 * 
		 * props.put("mail.smtp.ssl.protocols", "TLSv1.2");
		 */

		 
			
			  Properties props = new Properties(); 
			  props.put("mail.smtp.host", "smtp.gmail.com"); 
			  props.put("mail.smtp.port", "587");
			  props.put("mail.smtp.auth", "true");

			  props.put("mail.smtp.starttls.enable", "true");
			  props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        
			  javax.mail.Session session = javax.mail.Session.getInstance(props,
          new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
            	return new PasswordAuthentication("unimhk.net@gmail.com", "pas@1234");
            }
          });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("unimhk.net@gmail.com"));
            message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse("davidchenhk@hotmail.com, client@missionpick.com"));
                //InternetAddress.parse("davidchenhk@hotmail.com"));
            message.setSubject("Let's talk from Slash Generation");
            
            if (! isValidEmailAddress(email)) {
            	return "let_talk_error";
            }
            
            message.setText("Dears,"
                    + "\n\n Email from: " + email);
            
            Transport.send(message);

            //System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }   	
    	
        return "let_talk";
    }
    
    public static boolean isValidEmailAddress(String email) {
    	   boolean result = true;
    	   try {
    	      InternetAddress emailAddr = new InternetAddress(email);
    	      emailAddr.validate();
    	   } catch (AddressException ex) {
    	      result = false;
    	   }
    	   return result;
    	}    
}

package com.uni;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbizApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(DbizApplication.class, args);
	}

}

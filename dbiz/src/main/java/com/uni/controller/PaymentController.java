package com.uni.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

//import org.springframework.data.repository.query.Param;

import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
//import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.param.checkout.SessionCreateParams;
import com.uni.model.CreatePayment;
import com.uni.model.CreatePaymentResponse;
import com.stripe.model.checkout.Session;

import com.stripe.model.PaymentIntent;
import com.stripe.param.PaymentIntentCreateParams;

@RestController
public class PaymentController {

	@Value("${stripe.api.key}")
	private String stripeApiKey;		

    static Long calculateOrderAmount(Object[] items) {
      // Replace this constant with a calculation of the order's amount
      // Calculate the order total on the server to prevent
      // users from directly manipulating the amount on the client
      long amount = 1 * 100L;
      //items[0].toString()
      //setAmount(1 * 100L);	
      return amount;
    	//return 1400;
    }   

    @PostMapping("/create-payment-intent")
    public CreatePaymentResponse shopping_cart_create_payment_intent(@RequestBody CreatePayment createPayment 
    		) throws StripeException {
        // This is your real test secret API key.
        Stripe.apiKey = stripeApiKey;
 
        //CreatePayment postBody = gson.fromJson(request.body(), CreatePayment.class);
        //List<String> paymentMethodTypes = new ArrayList<>();
        //paymentMethodTypes.add("card");
        //paymentMethodTypes.add("eps");
        
        PaymentIntentCreateParams createParams = new PaymentIntentCreateParams.Builder()
        .setCurrency("hkd")
        .setAmount(createPayment.getAmount()*1L)
        .putMetadata("customerInformation", "name:" + createPayment.getName()
        + " tel:" + createPayment.getTel()
        + " email:" + createPayment.getEmail())
        //.setAmount(1 * 100L)
        //.addAllPaymentMethodType(paymentMethodTypes)
        .build();
        // Create a PaymentIntent with the order amount and currency
        PaymentIntent intent = PaymentIntent.create(createParams);        
        CreatePaymentResponse paymentResponse = new CreatePaymentResponse(intent.getClientSecret());        
        return paymentResponse;
		/*
		 * String YOUR_DOMAIN = "http://localhost:8080"; SessionCreateParams params =
		 * SessionCreateParams.builder()
		 * .addPaymentMethodType(SessionCreateParams.PaymentMethodType.CARD)
		 * .setMode(SessionCreateParams.Mode.PAYMENT) .setSuccessUrl(YOUR_DOMAIN +
		 * "/success.html") .setCancelUrl(YOUR_DOMAIN + "/cancel.html") .addLineItem(
		 * SessionCreateParams.LineItem.builder() .setQuantity(1L) // Provide the exact
		 * Price ID (e.g. pr_1234) of the product you want to sell .setPrice("pr_1234")
		 * .build()) .build();
		 * 
		 * Session session = Session.create(params);
		 * 
		 * return "redirect:" + session.getUrl();
		 */
        
    }    

}

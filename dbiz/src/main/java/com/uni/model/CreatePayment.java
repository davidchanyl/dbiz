package com.uni.model;

import com.google.gson.annotations.SerializedName;

public class CreatePayment {
/*  @SerializedName("items")
  Object[] items;
  public Object[] getItems() {
    return items;*/
	private Integer amount;
	private String name;
	private String email;
	private String tel;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	
	
  }